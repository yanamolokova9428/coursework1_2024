﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
namespace welcomeWindow
{
    internal class VictoryMusic
    {
        private MediaPlayer _mediaPlayer;
        private bool _isPlaying;

        public VictoryMusic()
        {
            _mediaPlayer = new MediaPlayer();
            _isPlaying = false;
        }
        public void Open(Uri mediaUri)
        {
            _mediaPlayer.Open(mediaUri);
        }
        public void Play()
        {
            if (_mediaPlayer.Source != null)
            {
                _mediaPlayer.Play();
                _isPlaying = true;
            }
        }
        public void Pause()
        {
            if (_isPlaying)
            {
                _mediaPlayer.Pause();
                _isPlaying = false;
            }
        }
        public void Stop()
        {
            _mediaPlayer.Stop();
            _isPlaying = false;
        }
        public void SetVolume(double volume)
        {
            _mediaPlayer.Volume = volume;
        }
        public bool IsPlaying()
        {
            return _isPlaying;
        }
    }
}
