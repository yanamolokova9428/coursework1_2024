﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace welcomeWindow
{
    internal class PuzzlePiece
    {
        public BitmapImage ImageSource { get; private set; }
        public int Row { get; set; }
        public int Column { get; set; }
        public PuzzlePiece(BitmapImage imageSource)
        {
            ImageSource = imageSource;
        }
        public PuzzlePiece(BitmapImage imageSource, int row, int column)
        {
            ImageSource = imageSource;
            Row = row;
            Column = column;
        }
        public void SetPosition(int row, int column)
        {
            Row = row;
            Column = column;
        }
        public override string ToString()
        {
            return $"Piece at Row {Row}, Column {Column}";
        }
    }
}
