﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace welcomeWindow
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void buttonGallery_Click(object sender, RoutedEventArgs e)
        {
            gallery galleryWindow = new gallery();
            galleryWindow.Show();
            this.Close();
        }
        private void recordsButton_Click(object sender, RoutedEventArgs e)
        {
            welcomeWindow.records recordsWindow = new welcomeWindow.records();
            recordsWindow.Show();
            this.Close();
        }
    }
}
