﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
namespace welcomeWindow
{
    /// <summary>
    /// Interaction logic for records.xaml
    /// </summary>
    public partial class records : Window
    {
        public records()
        {
            InitializeComponent();
            LoadRecords();
        }
        private void LoadRecords()
        {
            try
            {
                string[] lines = System.IO.File.ReadAllLines(@"C:\Users\Яна\source\repos\curs\records.txt");

                foreach (string line in lines)
                {
                    string[] parts = line.Split('\t');
                    if (parts.Length == 3)
                    {
                        recordsListView.Items.Add(new { PuzzleName = System.IO.Path.GetFileName(parts[2]), TimeTaken = parts[0], CompletionDate = parts[1] });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка у завантаженні: " + ex.Message);
            }
        }
        private void buttonBackToMenu_Click(object sender, RoutedEventArgs e)
        {
            welcomeWindow.MainWindow welcomeWindowInstance = new welcomeWindow.MainWindow();
            welcomeWindowInstance.Show();
            this.Close();
        }
    }
}
