﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Data;
using System.Linq;
using System.Collections.Generic;
namespace welcomeWindow
{
    public partial class game : Window
    {
        private BitmapImage _selectedImage;
        private DispatcherTimer _timer;
        private TimeSpan _time;
        private ObservableCollection<PuzzlePiece> _pieces;
        private Point _dragStartPoint;
        private VictoryMusic mediaPlayer;

        public game(BitmapImage selectedImage)
        {
            InitializeComponent();
            InitializeMediaPlayer();
            _selectedImage = selectedImage;
            dateLabel.Content = DateTime.Now.ToString("dd MMMM yyyy");
            _time = TimeSpan.Zero;
            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(1);
            _timer.Tick += Timer_Tick;
            _timer.Start();
            CutImageIntoPieces();
        }
        private void InitializeMediaPlayer()
        {
            mediaPlayer = new VictoryMusic();
            mediaPlayer.Open(new Uri(@"C:\Users\Яна\source\repos\curs\victory.m4a"));
        }
        private void PlayVictorySong()
        {
            if (mediaPlayer != null)
            {
                mediaPlayer.Play();
            }
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            _time = _time.Add(TimeSpan.FromSeconds(1));
            timerLabel.Content = _time.ToString(@"hh\:mm\:ss");
        }
        private void buttonPause_Click(object sender, RoutedEventArgs e)
        {
            if (_timer.IsEnabled)
            {
                _timer.Stop();
                buttonPause.Content = "Продовжити";
            }
            else
            {
                _timer.Start();
                buttonPause.Content = "Пауза";
            }
        }
        private void buttonBackToMenu_Click(object sender, RoutedEventArgs e)
        {
            gallery galleryWindow = new gallery();
            galleryWindow.Show();
            this.Close();
        }
        private void CutImageIntoPieces()
        {
            int rows = 4;
            int cols = 4;
            int pieceWidth = (int)_selectedImage.PixelWidth / cols;
            int pieceHeight = (int)_selectedImage.PixelHeight / rows;
            _pieces = new ObservableCollection<PuzzlePiece>();
            for (int y = 0; y < rows; y++)
            {
                for (int x = 0; x < cols; x++)
                {
                    Int32Rect rect = new Int32Rect(x * pieceWidth, y * pieceHeight, pieceWidth, pieceHeight);
                    CroppedBitmap croppedBitmap = new CroppedBitmap(_selectedImage, rect);

                    BitmapImage pieceImage = new BitmapImage();
                    using (MemoryStream stream = new MemoryStream())
                    {
                        BitmapEncoder encoder = new PngBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(croppedBitmap));
                        encoder.Save(stream);
                        pieceImage.BeginInit();
                        pieceImage.StreamSource = new MemoryStream(stream.ToArray());
                        pieceImage.EndInit();
                        pieceImage.Freeze();
                    }
                    PuzzlePiece piece = new PuzzlePiece(pieceImage, y, x);
                    _pieces.Add(piece);
                }
            }

            RandomizePieces();
        }
        private void RandomizePieces()
        {
            Random rnd = new Random();
            List<PuzzlePiece> piecesList = new List<PuzzlePiece>(_pieces);
            piecesList.Sort((a, b) => rnd.Next(-1, 2));
            _pieces = new ObservableCollection<PuzzlePiece>(piecesList);
            piecesListBox.ItemsSource = _pieces;
            DataTemplate template = new DataTemplate();
            FrameworkElementFactory factory = new FrameworkElementFactory(typeof(Image));
            factory.SetValue(Image.SourceProperty, new Binding("ImageSource"));
            template.VisualTree = factory;
            piecesListBox.ItemTemplate = template;
        }
        private void PiecesListBox_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _dragStartPoint = e.GetPosition(null);
            ListBox listBox = sender as ListBox;
            ListBoxItem listBoxItem = FindAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
            if (listBoxItem != null)
            {
                PuzzlePiece piece = (PuzzlePiece)listBox.ItemContainerGenerator.ItemFromContainer(listBoxItem);
                DataObject dragData = new DataObject("myFormat", piece);
                DragDrop.DoDragDrop(listBoxItem, dragData, DragDropEffects.Move);
                _pieces.Remove(piece);
            }
        }
        private void PiecesListBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                ListBox listBox = sender as ListBox;
                ListBoxItem listBoxItem = FindAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
                if (listBoxItem != null)
                {
                    Point currentPosition = e.GetPosition(null);
                    Vector diff = _dragStartPoint - currentPosition;
                    if (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                        Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance)
                    {
                        PuzzlePiece piece = (PuzzlePiece)listBox.ItemContainerGenerator.ItemFromContainer(listBoxItem);
                        DataObject dragData = new DataObject("myFormat", piece);
                        DragDrop.DoDragDrop(listBoxItem, dragData, DragDropEffects.Move);
                        _dragStartPoint = currentPosition;
                    }
                }
            }
        }
        private static T FindAncestor<T>(DependencyObject current) where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }
        private void PuzzleGrid_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent("myFormat"))
            {
                PuzzlePiece piece = e.Data.GetData("myFormat") as PuzzlePiece;
                Point dropPosition = e.GetPosition(puzzleGrid);
                int column = (int)(dropPosition.X / (puzzleGrid.Width / 4));
                int row = (int)(dropPosition.Y / (puzzleGrid.Height / 4));
                Image existingImage = puzzleGrid.Children.Cast<UIElement>().FirstOrDefault(child => child is Image && Grid.GetRow(child) == row && Grid.GetColumn(child) == column) as Image;
                if (existingImage == null)
                {
                    Image imageControl = new Image()
                    {
                        Width = puzzleGrid.Width / 4,
                        Height = puzzleGrid.Height / 4,
                        Source = piece.ImageSource,
                        Tag = piece
                    };
                    Grid.SetColumn(imageControl, column);
                    Grid.SetRow(imageControl, row);
                    puzzleGrid.Children.Add(imageControl);
                    _pieces.Remove(piece);
                    if (_pieces.Count == 0)
                    {
                        CheckCompletion();
                    }
                }
                else
                {
                    puzzleGrid.Children.Remove(existingImage);
                    Grid.SetColumn(existingImage, column);
                    Grid.SetRow(existingImage, row);
                    puzzleGrid.Children.Add(existingImage);
                }
            }
        }
        private void CheckCompletion()
        {
            bool isCorrect = true;
            foreach (UIElement child in puzzleGrid.Children)
            {
                if (child is Image img)
                {
                    int column = Grid.GetColumn(img);
                    int row = Grid.GetRow(img);
                    PuzzlePiece piece = img.Tag as PuzzlePiece;

                    if (piece.Row != row || piece.Column != column)
                    {
                        isCorrect = false;
                        break;
                    }
                }
            }

            if (isCorrect)
            {
                MessageBox.Show("🌸Вітаємо!🌸", "Перемога", MessageBoxButton.OK, MessageBoxImage.Information);
                PlayVictorySong();
                _timer.Stop();
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            mediaPlayer.Stop();
            MainWindow welcomeWindowInstance = new MainWindow();
            welcomeWindowInstance.Show();
            this.Close();
        }
        private void buttonSave_Click(object sender, RoutedEventArgs e)
        {
            string result = $"{_time.ToString(@"hh\:mm\:ss")}\t{DateTime.Now.ToString("dd MMMM yyyy")}\t{_selectedImage.UriSource}\n";
            File.AppendAllText(@"C:\Users\Яна\source\repos\curs\records.txt", result);
        }
    }
}
