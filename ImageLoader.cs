﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Imaging;
namespace welcomeWindow
{
    internal class ImageLoader
    {
        public List<ImageInfo> LoadImages(string imageDirectory)
        {
            List<ImageInfo> imagesList = new List<ImageInfo>();

            if (!Directory.Exists(imageDirectory))
            {
                throw new DirectoryNotFoundException("Папку не знайдено!");
            }
            foreach (string filename in Directory.GetFiles(imageDirectory, "*.jpg"))
            {
                try
                {
                    BitmapImage bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.UriSource = new Uri(filename);
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    bitmap.EndInit();
                    bitmap.Freeze();
                    string name = Path.GetFileNameWithoutExtension(filename);
                    imagesList.Add(new ImageInfo { Image = bitmap, Name = name });
                }
                catch (Exception imgEx)
                {
                    throw new IOException($"Помилка у завантаженні зображення {filename}: {imgEx.Message}", imgEx);
                }
            }
            return imagesList;
        }
    }
}
