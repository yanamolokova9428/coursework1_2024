﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Media.Imaging;
namespace welcomeWindow
{
    public partial class gallery : Window, INotifyPropertyChanged
    {
        private ImageInfo _selectedImage;
        private readonly ImageLoader _imageLoader;
        public event PropertyChangedEventHandler PropertyChanged;
        public gallery()
        {
            InitializeComponent();
            _imageLoader = new ImageLoader();
            LoadImages();
            DataContext = this;
            StartGameCommand = new RelayCommand(StartGame, CanStartGame);
        }
        public List<ImageInfo> ImagesList { get; } = new List<ImageInfo>();
        public ImageInfo SelectedImage
        {
            get => _selectedImage;
            set
            {
                _selectedImage = value;
                OnPropertyChanged(nameof(SelectedImage));
            }
        }
        public ICommand StartGameCommand { get; }
        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void LoadImages()
        {
            string imageDirectory = @"C:\Users\Яна\source\repos\curs\photoua";

            try
            {
                ImagesList.AddRange(_imageLoader.LoadImages(imageDirectory));
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Помилка у завантаженні зображень: {ex.Message}", "Помилка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool CanStartGame(object parameter)
        {
            return SelectedImage != null;
        }
        private void StartGame(object parameter)
        {
            game gameWindow = new game(SelectedImage.Image);
            gameWindow.Show();
            Close();
        }
        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
          
        }
        private void buttonBackToMenu_Click(object sender, RoutedEventArgs e)
        {
            welcomeWindow.MainWindow welcomeWindowInstance = new welcomeWindow.MainWindow();
            welcomeWindowInstance.Show();
            this.Close();
        }
    }
    public class RelayCommand : ICommand
    {
        private readonly Action<object> execute;
        private readonly Predicate<object> canExecute;
        public RelayCommand(Action<object> execute, Predicate<object> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public bool CanExecute(object parameter)
        {
            return canExecute == null || canExecute(parameter);
        }
        public void Execute(object parameter)
        {
            execute(parameter);
        }
    }
}
